﻿using System.Collections.Generic;

namespace FilesMergeSort
{
    interface ISorter
    {
        string SortFiles(List<string> filesPaths, string sortedMergedFilePath);
    }
}