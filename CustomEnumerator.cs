﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace FilesMergeSort
{
    public class CustomEnumerator<T> : IEnumerator<T>
    {
        private readonly IEnumerator<T> decoratedIEnum;

        public bool HasEnded { get; protected set; }

        public CustomEnumerator(IEnumerator<T> enumerator)
        {
            decoratedIEnum = enumerator;
        }

        public T Current => decoratedIEnum.Current;

        object IEnumerator.Current => ((IEnumerator)decoratedIEnum).Current;

        public void Dispose() => decoratedIEnum.Dispose();

        public bool MoveNext()
        {
            HasEnded = !decoratedIEnum.MoveNext();
            return !HasEnded;
        }

        public void Reset()
        {
            decoratedIEnum.Reset();
            HasEnded = false;
        }
    }
}
