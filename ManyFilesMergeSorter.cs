﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace FilesMergeSort
{
    class ManyFilesMergeSorter<T> : ISorter where T : IComparable
    {
        public ManyFilesMergeSorter(Func<string, T> convertFunc, bool isAscending)
        {
            this.convert = convertFunc;
            this.isAscending = isAscending;
        }

        private const string tmpPostfix = "_tmp";
        private const string tmpFilePartNameTemplate = "{0}_level_{1}_part_{2}";
        private const string invalidValueErrorTemplate = "{0} file contains invalid value: ({1}), that will be ignored";
        protected const int inMemorySortCount = 100;
        protected const int mergedFilesInSameTimeCount = 10;
        protected readonly bool isAscending = true;
        protected readonly Func<string, T> convert;
        
        protected DirectoryInfo rootTmpDirInfo;
        

        //  тут без рекурсий, т.к. глубина неизвестна и
        //  чтоб не менять размер стэка вызовов
        public string SortFiles
        (
            List<string> souceFilesPaths,
            string sortedMergedFilePath
        )
        {
            var rootTmpDirName = Environment.ProcessId + tmpPostfix;

            rootTmpDirInfo = Directory.CreateDirectory(rootTmpDirName);

            List<string> sortedInputFiles = new List<string>();
            foreach (var inFile in souceFilesPaths)
            {
                string str = SortFile(inFile);
                if (!string.IsNullOrEmpty(str))
                {
                    sortedInputFiles.Add(str);
                }
            }

            //  на случай, если файл всего один и не с чем его сливать
            if (sortedInputFiles.Count == 1)
            {
                File.Move(sortedInputFiles[0], sortedMergedFilePath);
            }
            else
            {
                sortedMergedFilePath = MergeFiles(sortedInputFiles, sortedMergedFilePath);
            }

            //  на всякий случай чистим неиспользуемую память
            GC.Collect();

            rootTmpDirInfo.Delete(true);
            
            sortedInputFiles.Clear();

            return sortedMergedFilePath;
        }

        protected void DeleteAll(IEnumerable<string> souceFilesPaths)
        {
            foreach (var filePath in souceFilesPaths)
                Delete(filePath);
        }

        protected void Delete(string filePath)
        {
            File.Delete(filePath);
        }

        protected string SortFile(string path)
        {
            //	проверяем, отсортирован ли уже файл в нужном порядке
            int currentSortOrder;
            if (CheckSorting(path, out currentSortOrder))
            {
                if
                (
                    currentSortOrder == 0 ||
                    (currentSortOrder > 0) == isAscending
                )
                {
                    return path;
                }
            }

            string tmpDirName = Path.GetFileName(path) + tmpPostfix;
            DirectoryInfo tmpDir = rootTmpDirInfo.CreateSubdirectory(tmpDirName);

            List<List<string>> levels = new List<List<string>> { new List<string>() };
            string sourceFileName = Path.GetFileNameWithoutExtension(path);

            //  разбиваем исходный файл на файлы приемлемого размера
            
            T[] lines = new T[inMemorySortCount];
            int counter = 0;
            using (CustomEnumerator<T> enmr = LinesEnumerator(path))
                while (!enmr.HasEnded)
                {
                    if (enmr.MoveNext() && counter < inMemorySortCount)
                    {
                        T value = enmr.Current;
                        lines[counter] = value;
                        counter++;
                    }
                    else
                    {
                        SortInMemory(lines, 0, counter);
                        int partNum = levels.Count > 0 ? levels[0].Count : 0;
                        string fileName = string.Format(tmpFilePartNameTemplate, sourceFileName, 0, partNum);
                        string partPath = Path.Combine(tmpDir.FullName, fileName);
                        SaveToFile(lines, 0, counter, partPath);
                        counter = 0;
                        levels[0].Add(partPath);
                        ClampTmpFiles(levels, sourceFileName);
                    }
                }

            List<string> allFiles = new List<string>();
            foreach(var lvl in levels)
                foreach (var fn in lvl)
                    allFiles.Add(fn);

            string sortedPath = Path.Combine(
                rootTmpDirInfo.FullName,
                Path.GetFileName(path) + "_sorted"
            );
            MergeFiles(allFiles, sortedPath);

            DeleteAll(allFiles);
            tmpDir.Delete(true);

            return sortedPath;
        }


        //  проверка, нет ли на каком-то уровне слишком большого
        //  количества файлов
        private void ClampTmpFiles(List<List<string>> levels, string sourceFileName)
        {
            // сливаем мелкие файлы если их стало слишком много
            for (int i = 0; i < levels.Count; i++)
            {
                if (levels[i].Count < mergedFilesInSameTimeCount) continue;
                if (i + 1 == levels.Count) levels.Add(new List<string>(mergedFilesInSameTimeCount));
                string fileName = string.Format(tmpFilePartNameTemplate, sourceFileName, i + 1, levels[i + 1].Count);
                levels[i + 1].Add(MergeFiles(levels[i], fileName));
                DeleteAll(levels[i]);
                levels[i].Clear();
            }
        }

        // классическая сортировка слиянием
        protected void SortInMemory(T[] arr, int low, int high)
        {
            int currentSectionLength = high - low;
            if (currentSectionLength <= 1)
                return;

            int mid = low + currentSectionLength / 2;

            SortInMemory(arr, low, mid);
            SortInMemory(arr, mid, high);

            T[] tmpArr = new T[currentSectionLength];
            int i = low, j = mid;
            for (int k = 0; k < currentSectionLength; k++)
            {
                //  если элементы в первой части закончились
                if (i == mid) tmpArr[k] = arr[j++];
                //  если элементы во второй части закончились
                else if (j == high) tmpArr[k] = arr[i++];
                //  сравниваем и вставляем с учётом порядка сортировки
                else if (arr[i].CompareTo(arr[j]) < 0)
                {
                    //  если первый меньше второго
                    if (isAscending) tmpArr[k] = arr[i++];
                    else tmpArr[k] = arr[j++];
                }
                else
                {
                    //  если первый больше второго
                    if (isAscending) tmpArr[k] = arr[j++];
                    else tmpArr[k] = arr[i++];
                }
            }

            for (int k = 0; k < currentSectionLength; k++)
            {
                arr[low + k] = tmpArr[k];
            }
        }
         
        protected void SaveToFile(T[] arr, int startIndex, int count, string path)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                for (int i = startIndex; i < count; i++)
                {
                    sw.WriteLine(arr[i]);
                }
            }
        }

        protected string MergeFiles(List<string> files, string resultFile)
        {
            if (files == null || files.Count == 0)
            {
                File.Create(resultFile).Dispose();
                return resultFile;
            }
            if (files.Count == 1)
            {
                File.Move(files[0], resultFile);
                return resultFile;
            }

            CustomEnumerator<T>[] enumsArr =
                new CustomEnumerator<T>[files.Count];

            for (int i = 0; i < files.Count; i++)
            {
                enumsArr[i] = LinesEnumerator(files[i]);
                enumsArr[i].MoveNext();
            }

            using (StreamWriter sw = new StreamWriter(resultFile))
            {
                bool done = false;
                while (!done)
                {
                    //  ищем среди файлов значение, удовлетворяющее порядку сортировки
                    int nextValueIndex = -1;
                    T value = default(T);

                    for (int i = 0; i < enumsArr.Length; i++)
                    {
                        if (!enumsArr[i].HasEnded)
                        {
                            if (nextValueIndex < 0)
                            {
                                nextValueIndex = i;
                                value = enumsArr[i].Current;
                            }
                            else
                            {
                                //  берём значение из другого файла, если оно больше
                                //  удовлетворяет порядку сортировки, чем текущее
                                if (isAscending)
                                {
                                    //	выбираем наименьший
                                    if (value.CompareTo(enumsArr[i].Current) > 0)
                                    {
                                        nextValueIndex = i;
                                        value = enumsArr[i].Current;
                                    }
                                }
                                else
                                {
                                    //	выбираем наибольший
                                    if (value.CompareTo(enumsArr[i].Current) < 0)
                                    {
                                        nextValueIndex = i;
                                        value = enumsArr[i].Current;
                                    }
                                }
                            }
                        }
                    }

                    if (nextValueIndex == -1)
                    {
                        done = true;
                        break;
                    }
                    else
                    {
                        enumsArr[nextValueIndex].MoveNext();
                    }

                    //  пишем в итоговый файл
                    sw.WriteLine(value);
                }
            }
            return resultFile;
        }

        protected CustomEnumerator<T> LinesEnumerator(string path)
        {
            return new CustomEnumerator<T>(
                GetFileLinesEnumerator(path)
            );
        }

        protected IEnumerator<T> GetFileLinesEnumerator(string path)
        {
            using (StreamReader sr = File.OpenText(path))
            {
                string line = String.Empty;
                while ((line = sr.ReadLine()) != null)
                {
                    T value;
                    try
                    {
                        value = convert(line);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(string.Format(invalidValueErrorTemplate, path, line));
                        continue;
                    }
                    yield return value;
                }
            }
        }

        protected bool CheckSorting(string path, out int sortOrder)
        {
            sortOrder = 0;

            bool sorted = true;
            int order = 0;
            int tmpOrder = 0;

            CustomEnumerator<T> enmr = LinesEnumerator(path);
            T lastValue;
            if (enmr.MoveNext())
            {
                lastValue = enmr.Current;
            }
            else
            {
                return sorted;
            }
            while (!enmr.HasEnded)
            {
                if (!enmr.MoveNext())
                    break;

                T tmpValue = enmr.Current;
                tmpOrder = tmpValue.CompareTo(lastValue);

                if (order != 0 && order != tmpOrder)
                {
                    sorted = false;
                    break;
                }


                order = tmpOrder;
                lastValue = tmpValue;
            }

            sortOrder = order;
            return sorted;
        }
    }
}